output "CloudFront" {
  value = "${aws_cloudfront_distribution.website_cdn.domain_name}"
}